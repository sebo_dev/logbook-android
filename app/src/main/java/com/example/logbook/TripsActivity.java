package com.example.logbook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class TripsActivity extends AppCompatActivity implements RecyclerViewAdapter.OnTriplistener {

    private static final String TAG= TripsActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trips);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        DatabaseManager dbManager = new DatabaseManager(TripsActivity.this.getApplicationContext());
        mAdapter = new RecyclerViewAdapter(dbManager.getAllTrips(),this);
        mRecyclerView.setAdapter(mAdapter);

        ///     ANSCHAUEN!!!!!!!!!!!!    https://www.youtube.com/watch?v=18VcnYN5_LM 

    }

    @Override
    public void onTripClick(int position) {
        Log.d(TAG,"LOL es funzt -"+position);

        Intent detailIntent = new Intent(this,TripDetailActivity.class);
        Position positionExtra = new Position(position);
        detailIntent.putExtra("position", positionExtra);
        startActivity(detailIntent);
    }
}