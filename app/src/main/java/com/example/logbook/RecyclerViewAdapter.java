package com.example.logbook;

import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Cursor dbcursor;
    private OnTriplistener mOnTriplistener;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView mID;
        public TextView mTripstart;
        public TextView mStartMilage;
        public TextView mEndMilage;

        OnTriplistener onTriplistener;

        public ViewHolder(View v, OnTriplistener onTriplistener) {
            super(v);
            mID = v.findViewById(R.id.ID);
            mTripstart = v.findViewById(R.id.TripStart);
            mStartMilage = v.findViewById(R.id.StartMilage);
            mEndMilage = v.findViewById(R.id.EndMilage);

            this.onTriplistener=onTriplistener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onTriplistener.onTripClick(getAdapterPosition());
        }
    }

    public interface OnTriplistener{
        void onTripClick(int position);
    }

    public RecyclerViewAdapter(Cursor myDataset, OnTriplistener onTriplistener) {
        this.dbcursor = myDataset;
        this.mOnTriplistener=onTriplistener;
    }

    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v =LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleitem_trip,parent,false);

        ViewHolder vh = new ViewHolder(v,mOnTriplistener);
        return vh;
    }

    public void onBindViewHolder(ViewHolder holder, int position) {

        dbcursor.moveToPosition(position);

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

        Date startDate= new Date(dbcursor.getLong(1));

        holder.mID.setText(dbcursor.getString(0));
        holder.mTripstart.setText(dateFormat.format(startDate));
        holder.mStartMilage.setText(dbcursor.getString(2)+"km");
        holder.mEndMilage.setText(dbcursor.getString(6)+"km");

    }

    public int getItemCount() {
        return dbcursor.getCount();
    }



}
