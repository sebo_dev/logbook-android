package com.example.logbook;

import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG= MapsActivity.class.getSimpleName();

    private GoogleMap mMap;

    Position position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        Bundle data = intent.getExtras();
        position = (Position) data.getParcelable("position");

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        DatabaseManager dbManager = new DatabaseManager(MapsActivity.this.getApplicationContext());
        Cursor dbCursor = dbManager.getAllTrips();

        dbCursor.moveToPosition(position.getPosition());

        //Log.d(TAG,"KOORDINATE: " +dbCursor.getFloat(3));

        // Add a marker in Sydney and move the camera
        LatLng startpoint = new LatLng(dbCursor.getFloat(3), dbCursor.getFloat(4));
        mMap.addMarker(new MarkerOptions().position(startpoint).title("StartPoint"));
        LatLng endpoint = new LatLng(dbCursor.getFloat(7), dbCursor.getFloat(8));
        mMap.addMarker(new MarkerOptions().position(endpoint).title("EndPoint").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(9));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(startpoint));

    }
}