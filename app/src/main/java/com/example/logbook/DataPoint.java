package com.example.logbook;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class DataPoint implements Parcelable {

    private static final String TAG=DataPoint.class.getSimpleName();

    public long timestamp;
    public long milage;
    public double Ncoordinate;
    public double Ecoordinate;

    public DataPoint(long timestamp, long milage, double Ncoordinate, double Ecoordinate){
        this.timestamp=timestamp;
        this.milage=milage;
        this.Ncoordinate=Ncoordinate;
        this.Ecoordinate=Ecoordinate;
    }

    public void logDataPoint() {
        Log.i(TAG,"Timestamp: "+timestamp+" milage: "+milage+" Ncoordinate: "+Ncoordinate+" Ecoordinate: "+Ecoordinate);
    }



    protected DataPoint(Parcel in) {
        timestamp = in.readLong();
        milage = in.readLong();
        Ncoordinate = in.readDouble();
        Ecoordinate = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(timestamp);
        dest.writeLong(milage);
        dest.writeDouble(Ncoordinate);
        dest.writeDouble(Ecoordinate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DataPoint> CREATOR = new Creator<DataPoint>() {
        @Override
        public DataPoint createFromParcel(Parcel in) {
            return new DataPoint(in);
        }

        @Override
        public DataPoint[] newArray(int size) {
            return new DataPoint[size];
        }
    };
}
