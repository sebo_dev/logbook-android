package com.example.logbook;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Dashboard extends AppCompatActivity implements SensorEventListener {

    private static final String TAG=Dashboard.class.getSimpleName();

    private SensorManager sensorManager;
    private Sensor ambient;

    User user;

    private void switchToSettings(User u){
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        settingsIntent.putExtra("user",u);
        startActivity(settingsIntent);
    }

    private void switchToStartActivity(){
        Intent startIntent = new Intent(this, StartActivity.class);
        startActivity(startIntent);
    }

    private void switchToTripsActivity(){
        Intent TripsIntent = new Intent(this, TripsActivity.class);
        startActivity(TripsIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        ambient = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

        final Button settingsButton = findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"clicked settings");

                switchToSettings(user);
            }
        });

        final Button TripsButton = findViewById(R.id.trips_button);
        TripsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"clicked TripsButton");

                switchToTripsActivity();
            }
        });

        FragmentManager fragmentManager=getSupportFragmentManager();
        UserFragment uFragment = (UserFragment)fragmentManager.findFragmentById(R.id.list);

        Intent intent = getIntent();
        if(intent.hasExtra("user")){
            Bundle data=intent.getExtras();
            user = (User) data.getParcelable("user");

            Log.i(TAG,"found User!!!!");
            user.loguser();
        }
        else{
            Log.i(TAG,"No EXTRA found");
            user = new User("EMPTY","EMPTY","EMPTY");
        }

        uFragment.setUser(user);

        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();


        editor.putString("user_name", user.getUsername());
        editor.putString("user_email", user.email);
        editor.putString("user_password", user.passwd);
        editor.commit();

        String homecountry= sharedPreferences.getString("homecountry","EARTH");
        Log.i(TAG,"HOMECOUNTRY ="+homecountry);






        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Start Trip", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                switchToStartActivity();
            }
        });
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float temperature = event.values[0];

        TextView temperatureText = (TextView)findViewById(R.id.temperature);

        temperatureText.setText(Float.toString(temperature)+"°C");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        // Register a listener for the sensor.
        super.onResume();
        sensorManager.registerListener(this, ambient, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // Be sure to unregister the sensor when the activity pauses.
        super.onPause();
        sensorManager.unregisterListener(this);
    }
}