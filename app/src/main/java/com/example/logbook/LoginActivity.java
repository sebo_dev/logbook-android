package com.example.logbook;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG= LoginActivity.class.getSimpleName();

    boolean founduser = false;

    User user;

    private void switchToRegister(){
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    private void switchToDashboard(){
        Intent dashboardIntent = new Intent(this,Dashboard.class);
        dashboardIntent.putExtra("user",user);
        startActivity(dashboardIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if(intent.hasExtra("user")){
            Bundle data=intent.getExtras();
            user = (User) data.getParcelable("user");

            Log.i(TAG,"found User!!!!");
            user.loguser();
            founduser=true;
        }else Log.i(TAG,"No EXTRA found");






        final Button registerButton = findViewById(R.id.register);
        registerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"clicked RegisterActivity");
                switchToRegister();

            }
        });

        final Button submitButton = findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"clicked submit");


                EditText email=(EditText)findViewById(R.id.email);
                EditText passwd=(EditText)findViewById(R.id.passwd);

                User loginuser = new User("NotNeeded",email.getText().toString(),passwd.getText().toString());
                loginuser.loguser();

                if(founduser==true){


                    if(user.checkequal(loginuser)==true){
                        Log.i(TAG,"Logging in!");

                        Context context = getApplicationContext();
                        CharSequence text = "Login successful!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();

                        switchToDashboard();
                    }else {
                        Log.i(TAG,"Login failed");

                        Context context = getApplicationContext();
                        CharSequence text = "Login failed!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();

                    }
                }

            }
        });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}