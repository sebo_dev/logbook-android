package com.example.logbook;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class User implements Parcelable {


    private static final String TAG=User.class.getSimpleName();

    public String name;
    public String email;
    public String passwd;


    public User(String name, String email, String passwd){
        this.name=name;
        this.email=email;
        this.passwd=passwd;
    }

    public String getUsername(){ return name; }

    public boolean checkequal(User u) {
        Log.v(TAG,"started comparing");
        Log.v(TAG,"Name "+u.name+"|"+name);
        Log.v(TAG,"Email "+u.email+"|"+email);
        Log.v(TAG,"Passwd "+u.passwd+"|"+passwd);
        if(u.email.equals(email)&&u.passwd.equals(passwd)){
            Log.i(TAG,"users match!");
            return true;
        }
        Log.i(TAG,"users DON'T match!");
        return false;
    }

    public void loguser() {
        Log.i(TAG,"Name: "+name+" Email: "+email+" Passwd: "+passwd);
    }

    //region Parcel

    protected User(Parcel in) {
        name = in.readString();
        email = in.readString();
        passwd = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(passwd);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };



    //endregion



}
