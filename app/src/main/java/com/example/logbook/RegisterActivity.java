package com.example.logbook;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG= RegisterActivity.class.getSimpleName();

    private void switchToLogin(User u){
        Intent mainActivityIntent = new Intent(this, LoginActivity.class);
        mainActivityIntent.putExtra("user",u);
        startActivity(mainActivityIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Button submitButton = findViewById(R.id.submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"clicked submit");

                EditText name=(EditText)findViewById(R.id.name);
                EditText email=(EditText)findViewById(R.id.email);
                EditText passwd=(EditText)findViewById(R.id.passwd);


                User user = new User(name.getText().toString(),email.getText().toString(),passwd.getText().toString());
                user.loguser();


                switchToLogin(user);

            }
        });
    }
}