package com.example.logbook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DatabaseManager extends SQLiteOpenHelper {

    private static final String TAG=DatabaseManager.class.getSimpleName();

    private static final String DATABASE_NAME = "TripData";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "Trips";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_timestampStart = "timestampStart";
    private static final String COLUMN_milageStart = "milageStart";
    private static final String COLUMN_NcoordinateStart = "NcoordinateStart";
    private static final String COLUMN_EcoordinateStart = "EcoordinateStart";
    private static final String COLUMN_timestampStop = "timestampStop";
    private static final String COLUMN_milageStop = "milageStop";
    private static final String COLUMN_NcoordinateStop = "NcoordinateStop";
    private static final String COLUMN_EcoordinateStop = "EcorrdinateStop";



    public DatabaseManager(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseManager(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public DatabaseManager(@Nullable Context context, @Nullable String name, int version, @NonNull SQLiteDatabase.OpenParams openParams) {
        super(context, name, version, openParams);
    }

    DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TABLE_NAME + " (\n" +
                "    " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "    " + COLUMN_timestampStart + " bigint NOT NULL,\n" +
                "    " + COLUMN_milageStart + " bigint NOT NULL,\n" +
                "    " + COLUMN_NcoordinateStart + " DOUBLE NOT NULL,\n" +
                "    " + COLUMN_EcoordinateStart + " DOUBLE NOT NULL,\n" +
                "    " + COLUMN_timestampStop + " bigint NOT NULL,\n" +
                "    " + COLUMN_milageStop + " bigint NOT NULL,\n" +
                "    " + COLUMN_NcoordinateStop + " DOUBLE NOT NULL,\n" +
                "    " + COLUMN_EcoordinateStop + " DOUBLE NOT NULL\n" +
                ");";

        db.execSQL(sql);
    }

    public boolean insertTrip(long timestamp, long milage, double Ncoordinate, double Ecoordinate, long timestamp2, long milage2, double Ncoordinate2, double Ecoordinate2){

        Log.i(TAG,timestamp+"|"+milage+"|"+Ncoordinate+"|"+Ecoordinate+"|"+timestamp2+"|"+milage2+"|"+Ncoordinate2+"|"+Ecoordinate2);

        ContentValues content = new ContentValues();
        content.put(COLUMN_timestampStart, timestamp);
        content.put(COLUMN_milageStart, milage);
        content.put(COLUMN_NcoordinateStart, Ncoordinate);
        content.put(COLUMN_EcoordinateStart, Ecoordinate);
        content.put(COLUMN_timestampStop, timestamp2);
        content.put(COLUMN_milageStop, milage2);
        content.put(COLUMN_NcoordinateStop, Ncoordinate2);
        content.put(COLUMN_EcoordinateStop, Ecoordinate2);
        SQLiteDatabase db = getWritableDatabase();
        return db.insert(TABLE_NAME,null , content) != -1;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public Cursor getAllTrips(){
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    }
}
