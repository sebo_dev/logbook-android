package com.example.logbook;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class UserFragment extends Fragment {

    private static final String TAG= UserFragment.class.getSimpleName();

    private TextView usernamebox;
    private TextView emailbox;

    private User u;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        usernamebox=(TextView) view.findViewById(R.id.usernamebox);
        emailbox=(TextView) view.findViewById(R.id.emailbox);
    }

    public void setUser(User user){
        this.u=user;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Dashboard activitäy = (Dashboard) getActivity();
        //u= activity.getUser();

        usernamebox.setText(u.getUsername());
        emailbox.setText(u.email);




        //u.loguser();
        Log.i(TAG,u.name);
    }


}