package com.example.logbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;

public class TripDetailActivity extends AppCompatActivity {

    private static final String TAG= TripDetailActivity.class.getSimpleName();

    private void switchToMapsActivity(Position cursorPosition){
        Intent mapsIntent = new Intent(this,MapsActivity.class);
        mapsIntent.putExtra("position", cursorPosition);
        startActivity(mapsIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_detail);



        final TextView StartTime = (TextView) findViewById(R.id.StartTime);
        final TextView EndTime = (TextView) findViewById(R.id.EndTime);
        final TextView MilageStart = (TextView) findViewById(R.id.MilageStart);
        final TextView MilageEnd = (TextView) findViewById(R.id.MilageEnd);
        final TextView Distance = (TextView) findViewById(R.id.Distance);
        final TextView nCoordStart = (TextView) findViewById(R.id.nCoordStart);
        final TextView eCoordStart = (TextView) findViewById(R.id.eCoordStart);
        final TextView nCoordEnd = (TextView) findViewById(R.id.nCoordEnd);
        final TextView eCoordEnd = (TextView) findViewById(R.id.eCoordEnd);


        Intent intent = getIntent();
        Bundle data = intent.getExtras();
        final Position position = (Position) data.getParcelable("position");

        final Button mapButton = findViewById(R.id.showMapButton);
        mapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"clicked MapButton");

                switchToMapsActivity(position);
            }
        });


        DatabaseManager dbManager = new DatabaseManager(TripDetailActivity.this.getApplicationContext());
        Cursor dbCursor = dbManager.getAllTrips();

        dbCursor.moveToPosition(position.getPosition());

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

        Date startDate= new Date(dbCursor.getLong(1));
        //Date startDate= new Date(1578256470);
        Date endDate= new Date(dbCursor.getLong(5));

        //Timestamp startTimestamp = new Timestamp(1578256470000);
        //Date startDate= new Date(startTimestamp.getTime());

        StartTime.setText(dateFormat.format(startDate));
        EndTime.setText(dateFormat.format(endDate));
        MilageStart.setText(dbCursor.getString(2)+"km");
        MilageEnd.setText(dbCursor.getString(6)+"km");
        Distance.setText(Integer.toString(dbCursor.getInt(6) - dbCursor.getInt(2))+"km");
        nCoordStart.setText("N:"+ dbCursor.getString(3));
        eCoordStart.setText("E:"+dbCursor.getString(4));
        nCoordEnd.setText("N:"+dbCursor.getString(7));
        eCoordEnd.setText("E:"+dbCursor.getString(8));



    }
}