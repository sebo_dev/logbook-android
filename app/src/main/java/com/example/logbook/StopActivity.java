package com.example.logbook;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class StopActivity extends AppCompatActivity {

    private static final String TAG= StopActivity.class.getSimpleName();

    DataPoint StartPoint;

    private LocationManager locationManager;


    private void switchToDashboard(){
        Intent dashboardIntent = new Intent(this,Dashboard.class);
        startActivity(dashboardIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop);

        final EditText milage = (EditText) findViewById(R.id.milageField);
        final EditText Ncoordinate = (EditText) findViewById(R.id.NcoordinateField);
        final EditText Ecoordinate = (EditText) findViewById(R.id.EcoordinateField);

        Intent intent = getIntent();
        Bundle data = intent.getExtras();
        StartPoint = (DataPoint) data.getParcelable("point");
        StartPoint.logDataPoint();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},1);

            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 1, new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                Ncoordinate.setText(String.valueOf(location.getLatitude()));
                Ecoordinate.setText(String.valueOf(location.getLongitude()));
                Log.d(TAG,"Ncoordinate: "+location.getLatitude());
                Log.d(TAG,"Ecoordinate: "+location.getLongitude());
                Log.d(TAG,"Accuracy: "+location.getAccuracy());

            }
        });


        final Button submitButton = findViewById(R.id.Button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "clicked StopTrip");


                //long tslong= System.currentTimeMillis()/1000;
                long tslong= System.currentTimeMillis()+3600000 ;

                long longmilage=Long.parseLong(milage.getText().toString());
                double longNcoordinate=Double.parseDouble(Ncoordinate.getText().toString());
                double longEcoordinate=Double.parseDouble(Ecoordinate.getText().toString());

                DatabaseManager dbManager = new DatabaseManager(StopActivity.this.getApplicationContext());
                dbManager.insertTrip(StartPoint.timestamp,StartPoint.milage,StartPoint.Ncoordinate,StartPoint.Ecoordinate,tslong,longmilage,longNcoordinate,longEcoordinate);

                Cursor cursor = dbManager.getAllTrips();
                cursor.moveToFirst();
                do {
                    Log.i(TAG, "ID: "                +cursor.getString(0)+
                                    " TimestampStart: "    +cursor.getString(1)+
                                    " MilageStart: "       +cursor.getString(2)+
                                    " NcoordinateStart: "  +cursor.getString(3)+
                                    " EcoordinateStart: "  +cursor.getString(4)+
                                    " TimestampStop: "     +cursor.getString(5)+
                                    " MilageStop: "        +cursor.getString(6)+
                                    " NcoordinateStop: "   +cursor.getString(7)+
                                    " EcoordinateStop: "   +cursor.getString(8));
                } while (cursor.moveToNext());


                switchToDashboard();

            }
        });



    }
}