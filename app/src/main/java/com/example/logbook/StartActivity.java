package com.example.logbook;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class StartActivity extends AppCompatActivity {

    private static final String TAG = StartActivity.class.getSimpleName();

    private LocationManager locationManager;

    private void switchToStopActivity(DataPoint point) {
        Intent stopIntent = new Intent(this, StopActivity.class);
        stopIntent.putExtra("point", point);
        startActivity(stopIntent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        final EditText milage = (EditText) findViewById(R.id.milageField);
        final EditText Ncoordinate = (EditText) findViewById(R.id.NcoordinateField);
        final EditText Ecoordinate = (EditText) findViewById(R.id.EcoordinateField);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},1);

            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 1, new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                Ncoordinate.setText(String.valueOf(location.getLatitude()));
                Ecoordinate.setText(String.valueOf(location.getLongitude()));
                Log.d(TAG,"Ncoordinate: "+location.getLatitude());
                Log.d(TAG,"Ecoordinate: "+location.getLongitude());
                Log.d(TAG,"Accuracy: "+location.getAccuracy());

            }
        });

        final Button submitButton = findViewById(R.id.Button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"clicked StartTrip");

                //long tslong= System.currentTimeMillis()/1000;
                long tslong= System.currentTimeMillis()+3600000;
                DataPoint point=new DataPoint(tslong,Long.parseLong(milage.getText().toString()),Double.parseDouble(Ncoordinate.getText().toString()),Double.parseDouble(Ecoordinate.getText().toString()));
                point.logDataPoint();

                switchToStopActivity(point);

            }
        });
    }
}