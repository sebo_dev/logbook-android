package com.example.logbook;


import android.os.Parcel;
import android.os.Parcelable;

public class Position implements Parcelable {

    int position;

    Position(int position){
        this.position=position;
    }

    protected Position(Parcel in) {
        position = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(position);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Position> CREATOR = new Creator<Position>() {
        @Override
        public Position createFromParcel(Parcel in) {
            return new Position(in);
        }

        @Override
        public Position[] newArray(int size) {
            return new Position[size];
        }
    };

    public int getPosition(){
        return this.position;
    }
}
